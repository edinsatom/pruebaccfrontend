import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuardGuard } from './services/guard.guard';

import { ProductosComponent } from './components/productos/productos.component';
import { CrearProductoComponent } from './components/crear-producto/crear-producto.component';
import { RegistroComponent } from './components/registro/registro.component';
import { PerfilComponent } from './components/perfil/perfil.component';


const routes: Routes = [
  { path: 'productos', component: ProductosComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'perfil', component: PerfilComponent, canActivate: [ GuardGuard ] },
  { path: 'producto/:id', component: CrearProductoComponent, canActivate: [ GuardGuard ] },
  { path: '**', pathMatch: 'full', redirectTo: 'productos' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
