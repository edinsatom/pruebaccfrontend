import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaisesService {

  url = 'https://restcountries.eu/rest/v2/all'

  constructor( private http:HttpClient) { }

  obtenerPaises(){

    return this.http.get( `${this.url}` )
      .pipe(
        map( resp => this.generarArreglo( resp ) )
      );
  }

  private generarArreglo( resp:object ){

    let paises:string[] = [];

    for (const name in resp) {
      if (resp.hasOwnProperty(name)) {
        paises.push(resp[name].name);
      }
    }

    return paises;
  }


}
