import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';

import { ProductoModel } from '../models/producto.model';
import { FileModel } from '../models/file.model';
import { UsuarioModel } from '../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  private CARPETA_IMG = 'img/';
  private url = 'https://pruebacc-597b7.firebaseio.com';

  constructor(private http: HttpClient, private storage: AngularFireStorage) {

  }

  getProductos() {

    return this.http.get(`${this.url}/productos.json`)
      .pipe(
        map(resp => this._generarArreglo(resp))
      );
  }
  
  uploadFile(imagen: FileModel) {
    const file = imagen.archivo;
    const filePath = `${this.CARPETA_IMG}${imagen.nombreArchivo}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
  }

  obtenerImagen(imagen: string) {
    const ref = this.storage.ref(`${this.CARPETA_IMG}${imagen}`);
    return ref.getDownloadURL();
  }


  crearProducto(producto: ProductoModel) {

    return this.http.post(`${this.url}/productos.json`, producto)
      .pipe(
        map((resp: any) => {
          producto.id = resp.name;
          this.uploadFile(producto.imagen);
          return producto;
        })
      );
  }

  actualizarProducto(producto: ProductoModel) {

    const producTemp = {
      ...producto
    };

    let id = producto.id;
    delete producTemp.id;

    return this.http.put(`${this.url}/productos/${producto.id}.json`, producTemp)
      .pipe(
        map((resp: any) => {
          producto.id = id;
          return producto;
        })
      );
  }

  getUsuario() {

    let usuario: UsuarioModel;

    return this.http.get(`${this.url}/usuario.json`)
      .pipe(
        map((resp: UsuarioModel) => {
          usuario = resp;
          return usuario;
        })
      );
  }

  getProducto(id: string) {

    return this.http.get(`${this.url}/productos/${id}.json`);
  }



  borrarProducto(producto: ProductoModel) {

    return this.http.delete(`${this.url}/productos/${producto.id}.json`);
  }

  private _generarArreglo(productosObj: object) {

    const productos: ProductoModel[] = [];

    if (productosObj === null) return [];

    Object.keys(productosObj).forEach(key => {
      const producto: ProductoModel = productosObj[key];
      producto.id = key;
      this.obtenerImagen(producto.imagen.nombreArchivo).subscribe(
        resp => {
          producto.imagen.url = resp;
        }
      )
      productos.push(producto);
    })


    return productos;
  }
}
