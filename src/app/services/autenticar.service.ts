import { Injectable } from '@angular/core';
import { UsuarioModel } from '../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class AutenticarService {

  public registrado = false;

  constructor() { }

  login( usuario:UsuarioModel ):boolean{
    if(usuario.nombre == 'admin' && usuario.password == '123456'){
      localStorage.setItem('token', 'token_de_usuario_12345');
      this.registrado = true;
      return true;
    }
    return false;
  }

  logout(){
    localStorage.removeItem('token');
    this.registrado = false;
  }

  autenticado():boolean{

    if(localStorage.getItem('token')) this.registrado = true;

    return this.registrado;
  }
}
