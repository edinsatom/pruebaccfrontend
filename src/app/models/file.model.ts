export class FileModel {
    archivo: File;
    nombreArchivo:string;
    tipo: string;
    url:string;

    constructor( archivo:File){
        this.archivo = archivo;
        this.nombreArchivo = archivo.name;
        this.tipo = archivo.type;
    }
}