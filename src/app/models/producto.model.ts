import { FileModel } from './file.model';

export class ProductoModel {
    id: string;
    nombre: string;
    caracteristicas: string;
    email: string;
    origen: string;
    precio: number = 0;
    lanzamiento: string;
    vendidas: number = 0;
    existencia: number = 0;
    imagen: FileModel;
}