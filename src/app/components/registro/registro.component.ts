import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UsuarioModel } from '../../models/usuario.model';
import { AutenticarService } from '../../services/autenticar.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styles: [`
    .invalid { color:red; font-size:12px}
  `]
})
export class RegistroComponent implements OnInit {

  usuario: UsuarioModel;

  constructor(private router: Router, public auth:AutenticarService ) { }

  ngOnInit(): void {
    this.usuario = new UsuarioModel( 'admin', '123456','','');
    this.usuario.password = '123456';

  }

  enviar( form:NgForm ){

    if( form.invalid ) return;

    if(this.auth.login( this.usuario ))
      this.router.navigateByUrl('/productos');
  }

}
