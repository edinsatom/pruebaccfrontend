import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { AutenticarService } from '../../services/autenticar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {

  constructor( private router: Router, public auth:AutenticarService ) { }

  ngOnInit(): void {
  }

  login(){
    this.router.navigateByUrl('/registro');
  }

  logout(){
    this.auth.logout();
    this.router.navigateByUrl('/home');
  }

}
