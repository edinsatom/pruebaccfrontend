import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ProductoModel } from '../../models/producto.model';
import { Observable } from 'rxjs';

import Swal from 'sweetalert2';

import { ProductosService } from '../../services/productos.service';
import { PaisesService } from '../../services/paises.service';
import { FileModel } from '../../models/file.model';

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styles: [`
    .invalid { color: red; font-size: 10px }
  `]
})
export class CrearProductoComponent implements OnInit {

  producto = new ProductoModel();
  paises = [];
  archivo:File;
  minPrecio = 5000;
  idProduct:string;
  nombreArchivo:string;
  errorArchivo:string;

  constructor(private paiService: PaisesService,
    private producService: ProductosService,
    private router: Router,
    private route: ActivatedRoute) { 

    }

  ngOnInit(): void {

    this.idProduct = this.route.snapshot.params.id;

    if( this.idProduct != 'nuevo') {
      this.producService.getProducto( this.idProduct )
        .subscribe( (resp:ProductoModel) => {
          this.producto = resp;
          this.producto.id = this.idProduct;
          this.producService.obtenerImagen(this.producto.imagen.nombreArchivo)
            .subscribe( resp => this.producto.imagen.url = resp);
        })
    }
    
    this.paiService.obtenerPaises()
      .subscribe(resp => {
        this.paises = resp;
      });

  }
  

  leerArchivo(event:any):boolean {
    if (event.target.files[0]) {
      this.archivo = event.target.files[0];
      this.producto.imagen = new FileModel( this.archivo );
    }
    return this.validaArchivo();
  }

  guardar(form: NgForm):void {

    let peticion: Observable<any>
    
    if ( !this.validacion() || form.invalid ) return;

    Swal.fire({
      title: 'Espere',
      text: 'Guardando información de producto',
      icon: 'info',
      allowOutsideClick: false
    });
    Swal.showLoading();

    if (!this.producto.id) {
      peticion = this.producService.crearProducto(this.producto);
    }
    else {
      peticion = this.producService.actualizarProducto(this.producto);
    }
    
    peticion.subscribe(resp => {
      
      Swal.fire({
        title: this.producto.nombre,
        text: 'Se actualizó correctamente.',
        icon: 'success'
      })

      this.router.navigateByUrl(`/productos`)
    })
  }

  actPrecio(e:string):void{

    this.producto.precio = Number(e);
    
  }

  validaArchivo():boolean{

    if ( !this.archivo ) {
      this.errorArchivo = 'Debe seleccionar un archivo';
      return false;
    }
    if ( this.archivo.type != 'image/jpeg' && this.archivo.type != 'image/png') {
      this.errorArchivo = 'Debe seleccionar un archivo jpg o png';
      return false;
    }
    if ( this.archivo.size/500000 > 1) {
      this.errorArchivo = 'El archivo debe tener un tamaña inferior a 1Mb';
      return false;
    }

    this.errorArchivo = '';
    return true;
  }

  private validacion( ):boolean{
    
    if ( !this.producto.id ){
      if(!this.validaArchivo()) return false;
    } 
    if ( !this.producto.origen ) return false;
    if ( this.producto.precio < this.minPrecio ) return false;
    if ( this.producto.vendidas < 0 ) return false;
    if ( this.producto.existencia < 0 ) return false;

    return true;
  }

}
