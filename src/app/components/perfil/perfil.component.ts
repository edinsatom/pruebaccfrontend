import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { ProductosService } from 'src/app/services/productos.service';
import { FileModel } from '../../models/file.model';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styles: []
})
export class PerfilComponent implements OnInit {

  archivo:FileModel;
  admin:UsuarioModel;
  loading = true;

  constructor( private prodService:ProductosService ) {

   }

  leerArchivo( event ){
    this.archivo = new FileModel( event.target.files[0] );
  }

  subirArchivo(){
    this.prodService.uploadFile( this.archivo );
    this.prodService.obtenerImagen('perfil.jpg').subscribe( resp => {
      this.admin.img = resp;
    });
  }

  ngOnInit(): void {
    this.prodService.getUsuario()
      .subscribe( (resp:UsuarioModel) => {
      this.admin = resp;
      this.prodService.obtenerImagen(this.admin.img).subscribe( resp => {
        this.admin.img = resp;
        this.loading = false;
      });
    });
  }

}
