import { Pipe, PipeTransform } from '@angular/core';
import { ProductoModel } from '../models/producto.model';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(value: ProductoModel[], buscado:string, opcion: number): ProductoModel[] {

    if(buscado.length < 2 ) {
      return value;
    }
    
    const resultados = [];
    const opciones = ['nombre', 'origen'];
    
    for(const elem of value){
      if( elem[opciones[opcion]].toLowerCase().indexOf(buscado.toLowerCase()) > -1 ) {
        resultados.push( elem );
      }
    }

    return resultados;
  }

}
