// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDFX90bCnApiSxSZ1CIeIe3s8Peyh89yL4",
    authDomain: "pruebacc-597b7.firebaseapp.com",
    databaseURL: "https://pruebacc-597b7.firebaseio.com",
    projectId: "pruebacc-597b7",
    storageBucket: "pruebacc-597b7.appspot.com",
    messagingSenderId: "985461035711",
    appId: "1:985461035711:web:2290bdc7ccac3c35fdec0a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
